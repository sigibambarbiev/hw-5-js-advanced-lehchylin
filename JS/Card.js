export class Card {
    constructor( user, userPosts ) {
        const { name, email, id } = user;
        const { title, body } = userPosts;
        this.name = name;
        this.email = email;
        this.id = id;
        this.title = title;
        this.body = body;
    }
}