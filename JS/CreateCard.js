import {usersJson} from "./Fetch.js";
import {userPostsJson} from "./Fetch.js";
import {NewCard} from "./NewCard.js";

usersJson.forEach(user => {

    userPostsJson.forEach(post => {

        (user.id === post.userId) && NewCard(user, post);

    });
});

export const button = document.querySelectorAll('button')