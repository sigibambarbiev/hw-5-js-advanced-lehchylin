import {URL_POSTS, URL_USERS} from "./Variables.js";

const users = await fetch(URL_USERS, {method: 'GET'});
export const usersJson = await users.json();

const userPosts = await fetch(URL_POSTS, {method: 'GET'});
export const userPostsJson = await  userPosts.json();

