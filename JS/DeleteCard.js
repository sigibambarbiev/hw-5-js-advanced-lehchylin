import {root, URL_POSTS} from "./Variables.js";
import {button} from "./CreateCard.js";

// button.forEach(btn => {
//     btn.addEventListener('click', elem => {
//
//         fetch(`${URL_POSTS}/${elem.target.parentElement.dataset.postId}`, {method: 'DELETE'})
//             .then(response => {
//                 (response.status === 200) && elem.target.parentElement.remove();
//             });
//     });
// });


root.addEventListener('click', elem => {

    if(elem.target.classList.contains('button')) {

        fetch(`${URL_POSTS}/${elem.target.parentElement.dataset.postId}`, {method: 'DELETE'})
            .then(response => {
                (response.status === 200) && elem.target.parentElement.remove();
            });
        
    }
})