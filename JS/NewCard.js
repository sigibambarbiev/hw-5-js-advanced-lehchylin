import {Card} from "./Card.js";
import {root} from "./Variables.js";

export function NewCard(user,post) {
    const newCard = new Card(user, post);

    const cardElement = document.createElement('div');
    root.append(cardElement);
    cardElement.classList.add('element');
    cardElement.dataset.postId = post.id;

    const cardHeader = document.createElement('div');
    cardElement.append(cardHeader);
    cardHeader.classList.add('card-header');

    const cardName = document.createElement('p');
    cardHeader.append(cardName);
    cardName.classList.add('name');
    cardName.innerText = newCard.name;

    const cardEmail = document.createElement('p');
    cardHeader.append(cardEmail);
    cardEmail.classList.add('email');
    cardEmail.innerText = newCard.email;

    const cardTitle = document.createElement('p');
    cardElement.append(cardTitle);
    cardTitle.classList.add('title');
    cardTitle.innerText = newCard.title;

    const cardBody = document.createElement('p');
    cardElement.append(cardBody);
    cardBody.classList.add('body');
    cardBody.innerText = newCard.body;

    const button = document.createElement('button');
    cardElement.append(button);
    button.classList.add('button');
    button.innerText = 'DELETE';
}